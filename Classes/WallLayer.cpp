//#include <Layer.h>
#include "WallLayer.h"
#include <vector>
#include <cmath>
#include <string>
#include <iostream>
#include <fstream>
#include <string.h>


using namespace cocos2d;
using namespace std;

USING_NS_CC;

CCScene* WallLayer::scene () {
	CCScene *scene = CCScene::create();
	scene->addChild(WallLayer::create());
	return scene;
}

bool WallLayer::init () {
    if (!CCLayer::init())
        return false;

	CCSize windowSize = CCDirector::sharedDirector()->getWinSize();
    this->setTouchEnabled(true);
    this->setAccelerometerEnabled(true);
    this->swipeThreshold = 50;
    this->accelerationThreshold = 2.5;

	//Background init color
	ccColor4B backgroundColor = {69, 183, 255, 255};
	CCLayerColor* background = CCLayerColor::create(backgroundColor);
	addChild(background, -2);

	//ground
	ccColor4B groundColor = {131, 179, 28, 255};
	CCLayerColor* groundLayer = CCLayerColor::create(groundColor);
	CCSize groundSize(windowSize.width, windowSize.height/2.5);
	groundLayer->setContentSize(groundSize);
	addChild(groundLayer);

    this->mainCharacter = AnimatedCharacter::createWithFile("simple_monster.json", "simple_monster.atlas", 1.0f);

    this->mainCharacter->setPosition(ccp(windowSize.width/2, windowSize.height/2));
	float h_ratio = windowSize.height / 1280;
	float w_ratio = windowSize.width / 768;
    float real_ratio = min(h_ratio, w_ratio);
//    skeletonNode->setScale(real_ratio);
    addChild(this->mainCharacter, 0);

	//Cloud image list (hardcoded pouah!)
	cloudAmount = 6;
	cloudImageList.push_back("cloud1.png");
	cloudImageList.push_back("cloud2.png");
	cloudImageList.push_back("cloud3.png");
	cloudImageList.push_back("cloud4.png");
	initClouds();

//	applyPreferences();
	scheduleUpdate();
	return true;
}

void WallLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
    CCSetIterator it = pTouches->begin();
    CCTouch* touch = (CCTouch*) (*it);

    CCPoint begin = touch->getStartLocation();
    CCPoint end	  = touch->getLocation();
    int distanceX = begin.x - end.x;
    int distanceY = begin.y - end.y;
    if(abs(distanceX) > swipeThreshold
            || abs(distanceY) > swipeThreshold)
    {
        CCLog("Swipe detected!");
        CCPoint direction = begin - end;

        direction.normalize();
        CCLog("onSwipe with %d %d", direction.x, direction.y);
        this->mainCharacter->onSwipe(direction);
    }
    else//It's a tap
    {
        CCLog("Tap detected!");
        this->mainCharacter->onTap(end);
    }
}

void WallLayer::update (float deltaTime) {

}

void WallLayer::initClouds()
{
	for(int i=0;i<cloudAmount;i++)
	{
		createNewCloud();
	}
}

void WallLayer::createNewCloud()
{

	//Duration of animation
	int duration = rand() % 50;
	duration += 50;

	//Image of the cloud
	int imageIndex = rand() % cloudImageList.size();
	char* image = cloudImageList[imageIndex];

	//Positionning on the top half
	CCSize windowSize = CCDirector::sharedDirector()->getWinSize();
	int h = rand() % (int)windowSize.height/2;
	h += (int)windowSize.height/2;


	CCSprite* newCloud = CCSprite::create(image);
	CCSize cSize = newCloud->getContentSize();
	newCloud->setPositionY(h);
	newCloud->setPositionX(-cSize.width);

	newCloud->runAction( CCSequence::create(
									CCMoveTo::create(duration,ccp(windowSize.width + cSize.width, h)),
									CCCallFunc::create(newCloud,
											callfunc_selector(CCSprite::removeFromParent)),
									CCCallFunc::create(this,
											callfunc_selector(WallLayer::createNewCloud)),
									NULL));
	addChild(newCloud, -1);
}

void WallLayer::applyPreferences()
{
	//FIXME: Read for android sharedprefs file
//	CCUserDefault::sharedUserDefault()->
	CCLog("XML File test %s", CCUserDefault::sharedUserDefault()->getXMLFilePath().c_str());
	if ( cocos2d::CCUserDefault::sharedUserDefault()->isXMLFileExist() )
	{
	    cocos2d::CCLog( "XML file exists!" );
	    CCLog("Accelerometer %d",CCUserDefault::sharedUserDefault()->getBoolForKey("com.me.Wallie.accelerometer"));
		if(CCUserDefault::sharedUserDefault()->getBoolForKey("com.me.Wallie.accelerator"))
		{
			setAccelerometerEnabled(true);
		}
		else
		{
			setAccelerometerEnabled(false);
		}
	}
	else
	{
	    cocos2d::CCLog( "XML doesn't exists!" );
	}
	CCLog("%s", "Applying preferences...");
}

void WallLayer::didAccelerate(CCAcceleration* pAcceleration)
{

	if(pAcceleration->x > accelerationThreshold
			|| pAcceleration->y > accelerationThreshold
			|| pAcceleration->z > accelerationThreshold)
	{
		CCLog("Acceleration animation for {x,y,z} : {%f, %f, %f}", pAcceleration->x,
																	pAcceleration->y,
																	pAcceleration->z);
		if(pAcceleration->x > 0)
		{
//			skeletonNode->setScaleX(-1);
		}
		else
		{
//			skeletonNode->setScale(1);
		}
	}
}
