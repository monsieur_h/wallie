#include "animatedcharacter.h"

USING_NS_CC;
using namespace spine;

//ctor
AnimatedCharacter::AnimatedCharacter(const char* skeletonDataFile, const char* atlasFile, float scale)
        : CCSkeletonAnimation(skeletonDataFile, atlasFile, scale) {
}

AnimatedCharacter* AnimatedCharacter::createWithFile(const char *skeletonDataFile, const char *atlasFile, float scale)
{
    AnimatedCharacter* node = new AnimatedCharacter(skeletonDataFile, atlasFile, scale);
    node->autorelease();
    node->initAnimations();
    return node;
}

void AnimatedCharacter::initAnimations()
{
    this->m_idleAnimationName = "idle";
    this->m_maximumDefaultIdleAnimation = 4;
    this->debugBones = false;

    //Parsing animation names
    int animCount  = this->skeleton->data->animationCount;
    for(int i=0 ; i < animCount ; i++)
    {
        const char* animName = this->skeleton->data->animations[i]->name;
        if(strstr(animName, "idle")) //Idle/random animations
        {
            m_idleAnimationList.push_back(animName);
            CCLog("Found animation [%s] and added to idle animation list...", animName);
        }

        if(strstr(animName, "important"))//Animation that won't be interrupted
        {
            m_unInterruptibleAnimationList.push_back(animName);
            CCLog("Found animation [%s] and added to uninterruptible animation list...", animName);
        }
    }

    //Adding un-interruptible animations
//    this->m_unInterruptibleAnimationList.push_back("onAccelerate");

    this->setAnimation(0, m_idleAnimationName.c_str(), true);
    this->update(0);
    this->scheduleUpdate();
}

void AnimatedCharacter::update(float deltaTime)
{
    CCSkeletonAnimation::update(deltaTime);
}

void AnimatedCharacter::setDefaultMix(float pTime)
{
    int animationCount = this->skeleton->data->animationCount;
    for(int i=0 ; i<animationCount ; i++)
    {
        for(int j=0; j<animationCount ; j++)
        {
            this->setMix(this->skeleton->data->animations[i]->name,
                             this->skeleton->data->animations[j]->name,
                             pTime);
        }
    }
}

void AnimatedCharacter::setIdleAnimation(std::string pIdleAnimationName)
{
    if(pIdleAnimationName.empty())
        return;

    this->m_idleAnimationName = pIdleAnimationName;
    this->setAnimation(0, this->m_idleAnimationName.c_str(), true);
}

void AnimatedCharacter::setNewIdleAnimation()
{
    if(!this->isInterruptible())
        return;

    int i = rand() % m_idleAnimationList.size();
    this->setAnimation(0, m_idleAnimationList[i], false);
    this->addAnimation(0, m_idleAnimationName.c_str(), true);
}

bool AnimatedCharacter::isInterruptible()
{
    spTrackEntry* entry = spAnimationState_getCurrent(this->state, 0);
    const char* currentAnimation = (entry && entry->animation) ? entry->animation->name : 0;
    for(int i=0 ; i<m_unInterruptibleAnimationList.size() ; i++)
    {
        const char* animationName = m_unInterruptibleAnimationList[i];
        if(!strcmp(animationName, currentAnimation))
            return false;
    }
    return true;
}

void AnimatedCharacter::onAnimationStateEvent(int trackIndex, spEventType type, spEvent *event, int loopCount)
{
    spTrackEntry* entry = spAnimationState_getCurrent(this->state, trackIndex);
    const char* animationName = (entry && entry->animation) ? entry->animation->name : 0;

    switch (type) {
    case ANIMATION_START:
        CCLog("%d start: %s", trackIndex, animationName);
        break;
    case ANIMATION_END:
        CCLog("%d end: %s", trackIndex, animationName);
        break;
    case ANIMATION_COMPLETE:
        CCLog("%d complete: %s, %d", trackIndex, animationName, loopCount);
        this->setScaleX(1);
        if(loopCount > m_maximumDefaultIdleAnimation)
        {
            this->setNewIdleAnimation();
        }

        break;
    case ANIMATION_EVENT:
        CCLog("%d event: %s, %s: %d, %f, %s", trackIndex, animationName, event->data->name, event->intValue, event->floatValue, event->stringValue);
        break;
    }
    fflush(stdout);
}

void AnimatedCharacter::onSwipe(cocos2d::CCPoint pDirection)
{
    if(!this->isInterruptible())
        return;

    if(abs(pDirection.x)>abs(pDirection.y))
    {
        this->lateralJump(ccp(pDirection.x, 0));
    }
    else
    {
        this->jump();
    }
}

void AnimatedCharacter::jump()
{
    if(!this->isInterruptible())
        return;

    this->setAnimation(0, "onSwipeUp", false);
    this->addAnimation(0, this->m_idleAnimationName.c_str(), true);
}

void AnimatedCharacter::onTap(cocos2d::CCPoint pPosition)
{
    if(!this->isInterruptible())
        return;

    if(this->boundingBox().containsPoint(pPosition))
    {
        this->setAnimation(0, "onTap", false);
        this->addAnimation(0, this->m_idleAnimationName.c_str(), true);
    }
    else
    {
        if(pPosition.x > this->getPosition().x)
        {
            this->setScaleX(1);
        }
        else
        {
            this->setScaleX(-1);
        }
        this->setAnimation(0, "onTapOut", false);
        this->addAnimation(0, m_idleAnimationName.c_str(), true);
    }
}

void AnimatedCharacter::lateralJump(cocos2d::CCPoint pDirection)
{
    if(pDirection.x > 0)
    {
        this->setScaleX(1);
    }
    else
    {
        this->setScaleX(-1);
    }
    this->setAnimation(0, "onSwipe", false);
    this->addAnimation(0, this->m_idleAnimationName.c_str(), true);
}

void AnimatedCharacter::onAccelerate(cocos2d::CCAcceleration* pAcceleration)
{
    if(!this->isInterruptible())
        return;

    if(pAcceleration->x > 0)
    {
        this->setScaleX(-1);
    }
    else
    {
        this->setScaleX(1);
    }
    this->setAnimation(0, "onAccelerate", false);
    this->addAnimation(0, m_idleAnimationName.c_str(), true);
}
