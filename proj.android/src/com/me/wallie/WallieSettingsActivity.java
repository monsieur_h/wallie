package com.me.wallie;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
//import com.google.analytics.tracking.android.EasyTracker;



import android.os.Bundle;
import android.preference.PreferenceActivity;

public class WallieSettingsActivity extends PreferenceActivity {

@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	SharedPreferences prefs = getSharedPreferences( "UserDefault.xml", MODE_WORLD_WRITEABLE );
	SharedPreferences.Editor editor = prefs.edit();
//	prefs.ge
	Log.d("XML", getFilesDir().toString());
//	this.
	
//	editor.putBoolean("OK", false);
//	if(prefs.getBoolean("OK", false))
//	{
//		Log.d("XML","Boolean was OK");
//	}
	editor.putString("TestString", "Ceci est un test");
	editor.commit();
	
	addPreferencesFromResource(R.xml.pref_general);
}
}
